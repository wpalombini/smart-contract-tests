const { assert } = require("chai");
const truffleAssert = require("truffle-assertions");
const MySmartContract = artifacts.require("./MySmartContract.sol");

contract("MySmartContract", (accounts) => {
  let mySmartContract;

  beforeEach(async () => {
    mySmartContract = await MySmartContract.deployed();
  });

  describe("deployment", async () => {
    it("deploys successfully", async () => {
      const address = await mySmartContract.address;
      
      assert.notEqual(address, 0x0);
      assert.notEqual(address, "");
      assert.notEqual(address, null);
      assert.notEqual(address, undefined);
    });

    it("has a name", async () => {
      const name = await mySmartContract.name();
      assert.equal(name, "My Smart Contract");
    });
  });

  describe("validators", async () => {
    it("insertRecord should validate string", async () => {
      await truffleAssert.reverts(
        mySmartContract.insertRecord.call("", 1),
        "Invalid string.",
      );
    });

    it("insertRecord should validate number", async () => {
      await truffleAssert.reverts(
        mySmartContract.insertRecord.call("my valid string", 0),
        "Invalid number.",
      );
    });

    it("insertRecord should require sender to be owner", async () => {
      await truffleAssert.reverts(
        mySmartContract.insertRecord.call("my valid string", 1, { from: accounts[1] }), // accounts[0] is the owner
        "You are not the owner."
      );
    });
  });

  describe("insertRecord", async () => {
    it("should save data correctly", async () => {
      const result = await mySmartContract.insertRecord("my valid string", 7);
      const event = result.logs[0].args;
      const identity = await mySmartContract.identity();
      const data = await mySmartContract.data(identity);
      
      assert.equal(data.id.toNumber(), identity.toNumber());
      assert.equal(data.description, "my valid string");
      assert.equal(data.total.toNumber(), 7);
      assert.equal(data.author, accounts[0]);

      assert.equal(event.id.toNumber(), identity.toNumber());
      assert.equal(event.description, "my valid string");
      assert.equal(event.total.toNumber(), 7);
      assert.equal(event.author, accounts[0]);
    });
  });
});