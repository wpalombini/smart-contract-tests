Steps to run the tests:

1 - have Truffle installed globally (npm i -g truffle)
2 - have Truffle Ganache running
3 - in the console, call truffle test
