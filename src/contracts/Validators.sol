pragma solidity >=0.4.21 <0.6.0;

contract ValidatorsContract {
    address owner;

    constructor() public {
        owner = msg.sender;
    }

    modifier onwershipRequired(address addr) {
        require(addr == owner, "You are not the owner.");
        _;
    }
}
