pragma solidity >=0.4.21 <0.6.0;
import "./Validators.sol";

contract MySmartContract is ValidatorsContract {
    string public name;
    uint public identity = 0;
    mapping(uint => CustomData) public data;

    struct CustomData {
        uint id;
        string description;
        uint total;
        address author;
    }

    event DataCreated(
        uint id,
        string description,
        uint total,
        address author
    );

    constructor() public {
        name = "My Smart Contract";
    }

    function insertRecord(string memory description, uint256 total) public onwershipRequired(msg.sender)
    {
        require(bytes(description).length > 0, "Invalid string.");

        require(total > 0, "Invalid number.");

        // Increments id
        identity ++;

        data[identity] = CustomData(identity, description, total, msg.sender);

        emit DataCreated(identity, description, total, msg.sender);
    }
}
